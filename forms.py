from flask_wtf import FlaskForm, RecaptchaField
from wtforms import StringField, SelectField, SelectMultipleField, BooleanField, IntegerField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length, InputRequired, EqualTo
from wtforms.widgets import PasswordInput


##Form de connexion
class form_signIn(FlaskForm):
    email = StringField('Email ', validators=[DataRequired()])
    motDePasse = StringField('Mot de passe ', widget=PasswordInput(hide_value=True))
    pass

##Form de création de compte
class form_signUp(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    fields = [("locataire", "locataire"),("proprietaire", "proprietaire")]
    typeCompte = SelectField('Type de compte', choices=fields)
    nom = StringField('Nom ', validators=[DataRequired()])
    prenom = StringField('Prénom ', validators=[DataRequired()])
    dateNaissance = DateField(validators=[DataRequired()])
    adresse = StringField('Adresse ', validators=[DataRequired()])
    numeroTelephone = StringField('Numéro de téléphone', validators=[DataRequired(),Length(min=10,max=10,message="le numéro de téléphone n'est pas valide")])
    motDePasse = StringField('Mot de passe ', widget=PasswordInput(hide_value=True), validators=[DataRequired()])
    confirmationMotDePasse = StringField('Confirmer le mot de passe ', widget=PasswordInput(hide_value=True), validators=[DataRequired(), EqualTo('motDePasse',message="Les mots de passe ne correpondent pas")])
    captcha = RecaptchaField()
    pass

##Form de changement de mot de passe après perte
class form_recoverPassword(FlaskForm):
    email = StringField('Email', validators=[DataRequired()])
    pass


##Form de changement du mot de passe utilisateur
class form_utilisateurChangePassword(FlaskForm):
    ancienmdp = StringField('Ancien mot de passe ', widget=PasswordInput(hide_value=True), validators=[DataRequired()])
    nouveaumdp = StringField('Nouveau mot de passe ', widget=PasswordInput(hide_value=True), validators=[DataRequired()])
    confirmermdp = StringField('Confirmer le mot de passe ', widget=PasswordInput(hide_value=True), validators=[DataRequired()])
    pass

##Form de changement des paramètre du compte utilisateur
class form_utilisateurChangeElement(FlaskForm): 
    email = StringField('Email')
    nom = StringField('Nom ')
    prenom = StringField('Prénom ')
    adresse = StringField('Adresse ')
    numeroTelephone = StringField('Numéro de téléphone', validators=[Length(min=10,max=10)])
    captcha = RecaptchaField()
    pass

class form_nouveauBail(FlaskForm):
    emailLocataire = StringField('Email Locataire')
    bien = SelectField('Selection du bien', coerce=int)
    prixLoyer = StringField('prix du loyer')
    jourPaiementLoyer = StringField('jour de paiment du loyer ')
    charges = StringField('Prix charges')
    typeCharges = StringField('Type de charges, (Eau, electricité, gaz)')
    dureePreavis = StringField('Durée du préavis en mois')
    dureeBail = StringField('Duree du bail en années')
    premierLoyerEntreeCalcule = BooleanField('Calcul automatique du premier loyer')
    prixPremierLoyerEntree = StringField('Cout du premier Loyer')
    DateEntreeLogement = DateField("Date d'entrée du logement")
    nomGarant = StringField("Nom du garant ")
    pass

class form_modifierBail(FlaskForm):
    prixLoyer = StringField('prix du loyer', validators=[DataRequired()])
    jourPaiementLoyer = StringField('jour de paiment du loyer ', validators=[DataRequired()])
    charges = StringField('Prix charges', validators=[DataRequired()])
    typeCharges = StringField('Type de charges, (Eau, electricité, gaz)')
    dureePreavis = StringField('Durée du préavis en mois')
    nomGarant = StringField("Nom du garant ", validators=[DataRequired()])
    pass

class form_nouveauBien(FlaskForm):
    idImmeuble = SelectField("Selectionner l'immeuble", coerce=int)
    adresse = StringField('Adresse')
    ville = StringField('Ville')
    codePostal = StringField('Code Postal')
    superficie = StringField('Superficie')
    nombrePieceHabitable = StringField('Nombre de piece habitable')
    garage = BooleanField('garage')
    cellier = BooleanField('cellier')
    placeParking = BooleanField('place de parking')
    pass

class form_modifierBien(FlaskForm):
    superficie = StringField("Superficie")
    nombrePieceHabitable = StringField("Nombre de pièces habitables")
    garage = BooleanField('garage')
    cellier = BooleanField('Cellier')
    placeParking = BooleanField('place de parking')

class form_nouveauImmeuble(FlaskForm):
    adresse = StringField("Adresse Immeuble", validators=[DataRequired()])
    ville = StringField("ville", validators=[DataRequired()])
    codePostal = StringField("Code Postal", validators=[DataRequired(), Length(min=5,max=5,message="Le code postal n'est pas correct")])
    nombreEtage = StringField("Nombre d'etage", validators=[DataRequired()])
    pass

class form_modifierImmeuble(FlaskForm):
    adresse = StringField("Adresse")
    ville = StringField("Ville")
    codePostal = StringField("Code Postal")
    nombreEtage = StringField("Nombre d'étage")
    pass

class form_nouveauPaiement(FlaskForm):
    locataire = SelectField('Selection du locataire du bail', coerce=int)
    paiementEffectue = BooleanField()
    quittance = BooleanField("Envoyer quittance automatiquement")
    pass

class form_validerPaiement(FlaskForm):
    genererQuittance =BooleanField('Générer quittance')
    pass


