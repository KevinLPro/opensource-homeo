from flask import Flask, render_template, request, redirect, flash, url_for, make_response
from flask_login import LoginManager,UserMixin, login_user, login_required, logout_user, current_user
from flask_mail import Mail, Message
from reportlab.pdfgen import canvas
from pbkdf2 import PBKDF2, crypt
from pprint import pprint 
from forms import *
from models import *
from datetime import date
import os
import string
import random
from config import Config
import pdfkit

#region init

app = Flask(__name__)
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'kazeefazhefja'
#Captcha keys
app.config['RECAPTCHA_PUBLIC_KEY'] = '6LeHpOwUAAAAABwVDcL_zxNLTnsbt8VuBRJUNkpm'
app.config['RECAPTCHA_PRIVATE_KEY'] = '6LeHpOwUAAAAANmo68CGmpqvirvA1DcRQD-EGkWc'
app.config['RECAPTCHA_OPTIONS'] = {'theme': 'white'}
#Initialisation du systeme de login flask
login_manager = LoginManager()
login_manager.init_app(app)
app.config.from_object(Config)

mail=Mail(app)

salt = 'AE321653104ADAFA'
iv = '42A8E3D2E7CB2A890CE648240BB8238B'
key = PBKDF2("Homeo.",salt).read(32)

@login_manager.user_loader
def load_user(user_id):
    return Utilisateur.get(id=user_id)

#Creation d'un directory pour la sauvagardes des quittances
upload_dir = os.path.join(app.instance_path, 'uploads')
os.makedirs(upload_dir, exist_ok=True)

#endregion

@app.route('/')
def home():
    return render_template('home.html')



#region logs
@app.route('/signIn', methods=['GET', 'POST', ])
def signIn():
    print("OMAO")
    form = form_signIn()
    if form.validate_on_submit():
        print('OOF')
        email = form.email.data
        motDePasse = form.motDePasse.data
        utilisateur = Utilisateur.select().where((Utilisateur.email == email) & (Utilisateur.motDePasse == motDePasse)).first()
        print(utilisateur)
        if(utilisateur == None):
            flash("L'email ou le mot de passe ne correspond pas")
        else:
            login_user(utilisateur)
            current_user.id = utilisateur.id
            if(utilisateur.role == "locataire"): #Aller sur le page de dediée en fonction du type d'utilisateur
                return redirect(url_for('accueilLocataire'))
            else:
                return redirect(url_for('accueilProprietaire'))
    return render_template('sign_in.html', form=form)

@app.route('/recoverPassword', methods=['GET', 'POST', ])
def recoverPassword():
    form = form_recoverPassword()
    if form.validate_on_submit():
        utilisateur = Utilisateur.select().where(Utilisateur.email == form.email.data).first()
        if utilisateur != None:   
            email = form.email.data
            letters = string.ascii_lowercase
            nouveaumotDePasse = ''.join(random.choice(letters) for i in range(10)) ####Géneration d'un mot de passse aleatoire avec les caractère ascii
            utilisateur.motDePasse = nouveaumotDePasse
            utilisateur.save()
            msg = Message('Changement de mot de passe', recipients=['kevin.leleu.pro@gmail.com'])
            msg.html = "<p> Cette email à été envoyé automatiquement par Homéo</p><p>-----------</p><p>Merci d'utiliser le service Homéo</p><p>Bonjour %s </p><p>Vous perdu votre mot de passe, en voici un temporaire : %s que vous pouvez changer en allant directement sur https://homeo.com </p><p> Merci de votre confiance et à bientôt sur Homeo</p>" % (utilisateur.prenom, nouveaumotDePasse)
            try:
                mail.send(msg)
            except Error:
                flash("Une erreur est survenu pendant l'envoi du mail, verifiez votre connexion Internet et reessayez")
            return redirect(url_for('signIn'))  
        else:
            flash("L'email ne correpond pas")
            return redirect(url_for('recoverPassword'))  
    return render_template('recover_password.html', form=form)

@app.route('/signUp', methods=['GET', 'POST', ])
def signUp():
    print("TEST")
    form = form_signUp()
    if form.validate_on_submit():
        print("ENTERED")
        email = form.email.data
        motDePasse = form.motDePasse.data
        motDePasseConfirmation = form.confirmationMotDePasse.data
        utilisateur = Utilisateur.select().where(Utilisateur.email == email)
        print(utilisateur)
        if utilisateur == None:
            nouvelUtilisateur = Utilisateur()
            nouvelUtilisateur.email = email
            nouvelUtilisateur.nom = form.nom.data
            nouvelUtilisateur.prenom = form.prenom.data
            nouvelUtilisateur.adresse = form.adresse.data
            nouvelUtilisateur.numeroTelephone = form.numeroTelephone.data
            nouvelUtilisateur.motDePasse = motDePasse
            nouvelUtilisateur.dateNaissance = form.dateNaissance.data
            nouvelUtilisateur.role = form.typeCompte.data
            nouvelUtilisateur.save()
            return redirect(url_for('signIn'))
        else:
            flash("Un compte existe déjà pour cette adresse mail")   
    else:
        for fieldname, errorMessage in form.errors.items():
            for err in errorMessage:
                flash(err)
    return render_template('sign_up.html', form=form)

#endregion

#region locataire

##Accueil de l'espace locataire
@app.route('/accueilLocataire')
@login_required
def accueilLocataire():
    return render_template('accueilLocataire.html')

#region informations
@app.route('/accueil/infoLocataire')
@login_required
def infoLocataire():
    utilisateur = Utilisateur.select().where(Utilisateur.id == current_user.id).first()
    print(utilisateur.nom)
    return render_template('infoLocataire.html',utilisateur=utilisateur)

@app.route('/accueilLocataire/info/changementMotDePasse', methods=['GET', 'POST', ])
@login_required
def changementMotDePasse():
    form = form_utilisateurChangePassword()
    return render_template()

@app.route('/accueilLocataire/info/suppresionCompte')
@login_required
def suppresionCompte():
    id = current_user.id
    logout_user()

@app.route('/locataire/bail')
@login_required
def bail():
    bail = Bail.select().join_from(Bail, Bien).where(Bail.idLocataire == current_user.id).first()
    return render_template("locataireBail.html", bail=bail)

#Liste les paiements pas encore validé par le proprietaire
@app.route('/locataire/payer')
@login_required
def payer():
    paiementNonValide = Paiement.select().join(Bail).where(Bail.idLocataire == current_user.id and (Paiement.paiementEffectue == False or Paiement.paiementEffectue == None))
    nbpaiement = len(paiementNonValide)
    return render_template("locatairePaiements.html", paiement=paiementNonValide,nbpaiement=nbpaiement)

@app.route('/locataire/retards')
@login_required
def retards():
    bail = Bail.select().join(Bien, on=Bail.idBien.alias('bien')).where(Bail.idLocataire == current_user.id).first()
    return render_template("locataireBail.html", bail=bail)

@app.route('/locataire/quittances')
@login_required
def quittances():
    bail = Bail.select(Utilisateur).join_from(Bail, Bien).where(Bail.idLocataire == current_user.id).first()
    return render_template("locataireBail.html", bail=bail)

##récupération des informations du proprietaire
@app.route('/locataire/proprietaire')
@login_required
def proprietaire():
    idproprietaire = Bail.select(Bail.idProprietaire).where(Bail.idLocataire == current_user.id).first()
    print(idproprietaire.idProprietaire)
    return render_template("locataireProprietaire.html", proprietaire=idproprietaire.idProprietaire)
#endregion

#endregion

#region proprietaire

##Accueil de l'espace proprietaire
@app.route('/espaceProprietaire')
@login_required
def accueilProprietaire():
    return render_template('accueilProprietaire.html')

@app.route('/espaceProprietaire/parametres')
@login_required
def parametreProprietaire():
    utilisateur = Utilisateur.select().where(Utilisateur.id == current_user.id).first()
    return render_template('infoProprietaire.html' ,utilisateur=utilisateur)

@app.route('/espaceProprietaire/modifierProprietaire', methods=['GET', 'POST', ])
@login_required
def modifierProprietaire():
    utilisateur = Utilisateur.select().where(Utilisateur.id == current_user.id).first()
    form = form_utilisateurChangeElement(obj=utilisateur)
    if form.validate_on_submit():
        form.populate_obj(utilisateur)
        utilisateur.save()
        return redirect(url_for('parametreProprietaire'))
    return render_template("modifierProprietaire.html", form=form)

@app.route('/espaceProprietaire/modifierMotDePasse', methods=['GET', 'POST', ])
@login_required
def modifierMotPasse():
    utilisateur = Utilisateur.select().where(Utilisateur.id == current_user.id).first()
    form = form_utilisateurChangePassword()
    if form.validate_on_submit():
        if utilisateur.motDePasse == form.ancienmdp.data:
            utilisateur.motDePasse = form.confirmermdp.data
            utilisateur.save()
        return redirect(url_for('parametreProprietaire'))
    return render_template("modifierMDPProprietaire.html", form=form)

#region bail

##Récupere une liste de tout les bails du proprietaire pour pouvoir les visionner et les modifier par la suite
@app.route('/espaceProprietaire/baux')
@login_required
def baux():
    bails = Bail.select(Bail.id, Bien.adresse).join(Bien, on=Bail.idBien.alias('bien')).where(Bail.idProprietaire == current_user.id).order_by(Bail.dateCreationBail.asc())
    nbBail = len(bails)
    return render_template("baux.html", bail=bails, nbBail=nbBail)

@app.route('/espaceProprietaire/baux/nouveauBail', methods=['GET', 'POST', ])
@login_required
def nouveauBail():
    form = form_nouveauBail()
    ##Affichage dans le drop down de biens ne possédant aucun bail
    listeAdresseBienLoue = Bail.select(Bien.adresse).join(Bien, on=Bail.idBien.alias("bien")).where(Bail.idProprietaire == current_user.id) #recupération de tout les bien déja loue
    listeBien = Bien.select(Bien.id, Bien.adresse).where(Bien.idUtilisateur == current_user.id)
    listeBienNonLouee = []
    for lsBien in listeBien:
        founded = False
        for lsBienLouee in listeAdresseBienLoue:
            if lsBien.adresse == lsBienLouee.bien.adresse:
                founded = True
        if founded == False:
            listeBienNonLouee.append(lsBien)
    form.bien.choices=[(i.id, i.adresse) for i in listeBienNonLouee] #Application du choix dynamique de logement
    if form.validate_on_submit():
        locataire = Utilisateur.select().where(Utilisateur.email == form.emailLocataire.data).first() ##Recherche de l'id du locataire grace à son adresse email qui est unique dans la base de données
        if locataire == None:
            locataire = creationUtilisateurGenerique(form.emailLocataire.data)
            msg = Message('Création nouveau compte', recipients=[form.emailLocataire.data])
            msg.html = '<h4>Bienvenu sur homeo</h4><p>Bonjour, vous avez été inscrit sur la plateforme Homeo vous pouvez vous connecter grâce à votre adresse et le mot de passe suivant : %s </p>' % (locataire.motDePasse, )
            locataireID = locataire.id
            try:
                mail.send(msg)
            except Error:
                flash("Une erreur s'est produit pendant l'envoi du mail, email incorrect") 
        else: 
            locataireID = locataire.id
        if form.premierLoyerEntreeCalcule.data == True : ##Si l'utilisateur decide de faire calculer automatiquement le premier loyer, on lock le champs qui permet de preciser le prix       
            dateEntree = form.DateEntreeLogement.data
            if dateEntree.day < int(form.jourPaiementLoyer.data): ##Calcul du nombre de jour si dans le même mois que la prochaine date de paiement
                nombreJour = int(form.jourPaiementLoyer.data) - dateEntree.day 
            if dateEntree.day >= int(form.jourPaiementLoyer.data): ###Si le jour de paiement est le mois d'apres
                dateGenere = dateEntree
                dateGenere.replace(day=int(form.jourPaiementLoyer.data), month=dateGenere.month+1)
                nombreJour = dateGenere - dateEntree
            loyerParJour = int(form.prixLoyer.data) / 30.41 ##Nombre Moyen de jour par mois nb 
            calculLoyer = nombreJour * loyerParJour
        else:
            calculLoyer = form.prixPremierLoyerEntree.data
        bail = Bail()
        form.populate_obj(bail)
        bail.idLocataire = locataireID
        bail.idProprietaire = current_user.id
        bail.idBien = form.bien.data
        bail.prixLoyerEntree = calculLoyer
        bail.dateCreationBail = date.today()
        bail.save()
        #Creation du premier paiement qui va servir à générer les suivants
        paiement = Paiement()
        paiement.idBail = bail.id
        dateEntree = form.DateEntreeLogement.data
        dateGenere = dateEntree
        if(dateEntree.day < int(form.jourPaiementLoyer.data)):
            dateGenere.replace(day=int(form.jourPaiementLoyer.data))
        else:
            dateGenere.replace(day=int(form.jourPaiementLoyer.data), month=dateEntree.month+1)
        paiement.datePaiement = dateGenere
        paiement.paiementEffectue = False
        paiement.prixPaiement = calculLoyer
        paiement.save()
        return redirect(url_for('baux'))        
    return render_template("nouveauBail.html", form=form)
        
@app.route('/espaceProprietaire/baux/visionner', methods=['GET', 'POST', ])
@login_required
def visionnerBail():
    idBail = request.args.get('idBail') ##Recupere l'id du bail qui doit être affiché
    bail = Bail.select().where(Bail.id == idBail).first()
    return render_template('visionnerBail.html', bail=bail)

@app.route('/espaceProprietaire/baux/visionner/modifier', methods=['GET', 'POST', ])
@login_required
def modifierBail():
    idBail = request.args.get('idBail') ##Recupere l'id du bail qui doit être affiché
    bail = Bail.select().where(Bail.id == idBail).first() ##Recuperation du bail pour affecter des valeurs par défaut
    form = form_modifierBail(obj=bail)
    if form.validate_on_submit():
        form.populate_obj(bail)
        bail.save()
        return redirect(url_for('visionnerBail', idBail = bail.id))   
    return render_template('modifierBail.html', form=form)

@app.route('/espaceProprietaire/baux/visionner/supprimer', methods=['GET', 'POST', ])
@login_required
def supprimerBail():
    idBail = request.args.get('linkFlux') ##Recupere l'id du bail qui doit être affiché
    Bail.delete().where(Bail.id == idBail)

#endregion

#region Bien
@app.route('/espaceProprietaire/biens', methods=['GET', 'POST', ])
@login_required
def biens():
    biens = Bien.select().where(Bien.idUtilisateur == current_user.id)
    nbBien = len(biens)
    return render_template('biens.html',biens=biens, nbBien=nbBien)

@app.route('/espaceProprietaire/biens/nouveauBien', methods=['GET', 'POST', ])
@login_required
def nouveauBien():
    form = form_nouveauBien()
    form.idImmeuble.choices =[("1","---")]+[(i.id, i.adresse) for i in Immeuble.select().where(Immeuble.idProprietaire == current_user.id)]
    if form.validate_on_submit():
        bien = Bien()
        form.populate_obj(bien)
        bien.idUtilisateur = current_user.id
        bien.save()
        return redirect(url_for('biens'))
    return render_template('nouveauBien.html',form=form)

@app.route('/espaceProprietaire/biens/visionner', methods=['GET', 'POST'])
@login_required
def visionnerBien():
    idBien = request.args.get('idBien')
    bien = Bien.select().where(Bien.id == idBien).first()
    return render_template("visionnerBien.html",bien=bien)

@app.route('/espaceProprietaire/biens/modifier', methods=['GET','POST'])
@login_required
def modifierBien():
    idBien = request.args.get('idBien')
    bien = Bien.select().where(Bien.id == idBien).first()
    form = form_modifierBien(obj=bien)
    if form.validate_on_submit():
        form.populate_obj(bien)
        bien.save()
        return redirect(url_for('visionnerBien'))
    return render_template("modifierBien.html", form=form)
#endregion

#region immeuble
@app.route('/espaceProprietaire/immeubles', methods=['GET', 'POST', ])
@login_required
def immeubles():
    immeubles = Immeuble.select().where(Immeuble.idProprietaire == current_user.id)
    nbImmeuble = len(immeubles)
    return render_template("immeubles.html", immeubles=immeubles, nbImmeuble=nbImmeuble)


@app.route('/espaceProprietaire/immeubles/ajouter', methods=['GET', 'POST', ])
@login_required
def nouveauImmeuble():
    form = form_nouveauImmeuble()
    if form.validate_on_submit():
        immeuble = Immeuble()
        form.populate_obj(immeuble)
        immeuble.idProprietaire = current_user.id
        immeuble.save()
        return redirect(url_for('immeubles'))
    return render_template("nouveauImmeuble.html", form=form)

###PROBLEME DE CHANGER DE PARAMETRE LE DEFAULT N'EST PAS ENCORE PRIS
@app.route('/espaceProprietaire/immeubles/modifier', methods=['GET', 'POST', ])
@login_required
def modifierImmeuble():
    idImmeuble = request.args.get('idImmeuble')#Recupération de l'id de l'immeuble selectionné
    immeuble = Immeuble.select().where(Immeuble.id == idImmeuble).first()
    form = form_modifierImmeuble(obj=immeuble)
    if form.validate_on_submit():
        form.populate_obj(immeuble)
        immeuble.save()
        return redirect(url_for('immeubles'))
    return render_template("modifierImmeuble.html", form=form, immeuble=immeuble)

@app.route('/espaceProprietaire/immeubles/visionner', methods=['GET', 'POST', ])
@login_required
def visionnerImmeuble():
    idImmeuble = request.args.get('idImmeuble')#Recupération de l'id de l'immeuble selectionné
    immeuble = Immeuble.select().where(Immeuble.id == idImmeuble).first()
    utilisateur = Utilisateur.select().where(Utilisateur.id == current_user.id).first()
    return render_template("visionnerImmeuble.html", immeuble=immeuble, utilisateur=utilisateur)

@app.route('/espaceProprietaire/immeubles/visionner', methods=['GET', 'POST', ])
@login_required
def supprimerImmeuble():
    return render_template()
#endregion

#region paiement

@app.route('/espaceProprietaire/paiements', methods=['GET', 'POST', ])
@login_required
def paiements():    
    paiements = Paiement.select().join_from(Paiement, Bail).join(Utilisateur, on=Bail.idLocataire.alias('locataire')).where(Bail.idProprietaire == current_user.id)
    nbPaiement = len(paiements)
    return render_template("paiements.html", paiements=paiements,nbPaiement=nbPaiement)

@app.route('/espaceProprietaire/paiements/validerUnPaiement', methods=['GET', 'POST'])
@login_required
def validationPaiement():
    idPaiement = request.args.get("idPaiement")
    form = form_validerPaiement()
    paiements = Paiement.select(Paiement.id, Paiement.datePaiement).join_from(Paiement, Bail).join(Utilisateur, on=Bail.idLocataire.alias('locataire')).where(Paiement.id == idPaiement).first()
    paiement = Paiement.select().where(Paiement.id == idPaiement).first()
    print(idPaiement)
    quittance = Quittance.select().where(Quittance.idPaiement == idPaiement).first()
    print(paiement)
    if form.validate_on_submit():
        if form.genererQuittance == True:
            generationQuittance()
        paiement.paiementEffectue = True
        paiement.save()
        return redirect(url_for('validationPaiement'))
    return render_template('validationPaiement.html', paiements=paiements, form=form, quittance=quittance)


@app.route('/espaceProprietaire/genererQuittance', methods=['GET', 'POST'])
@login_required
def genererQuittance():
    idPaiement = request.args.get('idPaiement')
    bail = Bail.select().join(Utilisateur, on=Bail.idLocataire.alias('locataire')).where(Bail.idProprietaire == current_user.id) ##Recuperation bail + locataire
    paiement = Paiement.select().join_from(Paiement, Bail).join(Utilisateur, on=Bail.idLocataire.alias('locataire')).where(Bail.idLocataire == current_user.id)
    proprietaire = Utilisateur.select().where(Utilisateur.id == current_user.id)
    renderedQuittance = render_template('template_quittance.html',bail=bail,paiement=paiement,proprietaire=proprietaire)
    ##Debut de la génération du pdf
    retour = genererQuittance(renderedQuittance, bail.locataire.nom, date.today())
    print(retour.filename)
    quittance = Quittance()
    quittance.idPaiement = paiements.id
    quittance.dateQuittance = datetime.datetime.now()
    quittance.fichierQuittance = fichier
    quittance.save()

@app.route('/espaceProprietaire/retards', methods=['GET', 'POST'])
@login_required
def visionnerRetard():
    retards = Paiement.select().join_from(Paiement, Bail).join(Utilisateur, on=Bail.idLocataire.alias('locataire')).where(Bail.idProprietaire == current_user.id and Paiement.datePaiement < datetime.datetime.now() and Paiement.paiementEffectue == False)
    nbretards = len(retards)
    return render_template("visionnerRetards.html", retards=retards, nbretards=nbretards)
#endregion

#endregion

#region functionGenerique

"""Fonction de création d'un utilisateur générique.

Arguments:
email : email de l'utilisateur à créer
"""
def creationUtilisateurGenerique(email):
    letters = string.ascii_lowercase
    motDePasse = ''.join(random.choice(letters) for i in range(10)) ####Géneration d'un mot de passse aleatoire avec les caractère ascii
    nouvelUtilisateur = Utilisateur()
    nouvelUtilisateur.email = email
    nouvelUtilisateur.nom = "XXX"
    nouvelUtilisateur.prenom = "XXX"
    nouvelUtilisateur.adresse = "XXX"
    nouvelUtilisateur.numeroTelephone = "XXXXXXXXXX"
    nouvelUtilisateur.motDePasse = motDePasse
    nouvelUtilisateur.dateNaissance = date.today()
    nouvelUtilisateur.role = "locataire"
    nouvelUtilisateur.save()
    return nouvelUtilisateur

def generationQuittance(render, nomLocataire, dateQuittance):
    pdf = pdfkit.from_string(render, False)
    response = make_response(pdf)
    response.headers['Content-Type'] = 'application/pdf'
    response.headers['Content-Disposition'] = 'attachment; filename=' + nomLocataire + '-' + dateQuittance + '.pdf'
    return response

def send():
    msg = Message('SUjet de mon mail', recipients=['kevin.leleu.pro@gmail.com'])
    msg.body = 'Le jolie coprs de mon mail'
    msg.html = 'Le coprs facon <p>html</p>'
    mail.send(msg)
    return "mESSAGE ENVOYE"

##Deconnexion de l'utilisateur en cours lorsque le bouton de deconnexion est pressé
@app.route('/deconnexion')
@login_required
def deconnexion():
    logout_user()
    return redirect(url_for('home'))

#endregion

#region cliCommand

@app.cli.command()
def initdb():
    create_tables()

@app.cli.command()
def dropdb():
    drop_tables()

#endregion