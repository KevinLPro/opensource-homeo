from peewee import *
from flask_login import UserMixin

database = SqliteDatabase("database.sqlite3")

class BaseModel(Model):
    class Meta: database = database



class Utilisateur(BaseModel, UserMixin):
    nom = CharField(null=False)
    prenom = CharField(null=False)
    adresse = CharField(null=False)
    numeroTelephone = CharField()
    motDePasse = CharField(null=False)
    email = CharField(null=False)
    dateNaissance = DateField(null=False)
    role = CharField(null=False)

class Immeuble(BaseModel):
    idProprietaire = ForeignKeyField(Utilisateur, null=False)
    adresse = CharField(null=False)
    ville = CharField(null=False)
    codePostal = CharField(null=False)
    nombreEtage = CharField(null=False)
    

class Bien(BaseModel):
    idUtilisateur = ForeignKeyField(Utilisateur, null=False)
    idImmeuble = ForeignKeyField(Immeuble, null=True) ##Un bien n'est pas forcément liée a un immeuble
    adresse = CharField(null=False)
    ville = CharField(null=False)
    codePostal = CharField(null=False)
    superficie = IntegerField(null=False)
    nombrePieceHabitable = IntegerField(null=False)
    garage = BooleanField()
    cellier = BooleanField()
    placeParking = BooleanField()

class Bail(BaseModel):
    idLocataire = ForeignKeyField(Utilisateur, null=False)
    idProprietaire = ForeignKeyField(Utilisateur, null=False)
    idBien = ForeignKeyField(Bien, null=False)
    prixLoyer = IntegerField(null=False)
    jourPaiementLoyer = CharField(null=False)
    charges = IntegerField(null=False)
    typeCharges = CharField(null=False)
    dureePreavis = IntegerField(null=False)
    prixLoyerEntree = FloatField(null=False)
    DateEntreeLogement = DateField(null=False)
    nomGarant = CharField(null=False)
    dateCreationBail = DateField(null=False)
    dureeBail = CharField(null=False)

class Paiement(BaseModel):
    idBail = ForeignKeyField(Bail, null=False)
    datePaiement = DateField(null=False)
    paiementEffectue = BooleanField()
    prixPaiement = CharField(null=False)

class Quittance(BaseModel):
    idPaiement = ForeignKeyField(Paiement, null=False)
    dateQuittance = DateField(null=False)
    fichierQuittance = BlobField(null=False)


def create_tables():
    with database:
        database.create_tables([Utilisateur, Bien, Bail, Paiement, Quittance, Immeuble])

def drop_tables():
    with database:
        database.drop_tables([Utilisateur, Bien, Bail, Paiement, Quittance, Immeuble])